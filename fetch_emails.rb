# Librairie native pour le protocol IMAP
require 'net/imap'
# Librairie externe permettant de facilement stocker des infos en variables
# d'environnement (ENV['QUELQUECHOSE'])
require 'dotenv/load'

# Début d'un bloc de capture d'erreur
begin
  # Création du socket
  imap = Net::IMAP.new ENV['IMAP_HOST'], ENV['IMAP_PORT'], false
  # Authentification en tant qu'administrateur
  imap.authenticate 'PLAIN', ENV['IMAP_LOGIN'], ENV['IMAP_PASSWORD']

  # Accès à toutes les boites
  list = imap.list '', '*'
  # Itération sur l'intégralité des boites
  list.each do |mailbox|
    # Selection de la mailbox
    imap.examine mailbox.name
    # Itération sur l'intégralité des mails de la boite
    imap.search(['ALL']).each do |message_id|
      # Accès à l'envlope du mail
      env = imap.fetch(message_id, 'ENVELOPE')[0].attr['ENVELOPE']
      # Affichage du champ 'To:', pour vérifier qu'on accès bien aux mails
      # de différentes boites mails
      puts env.to.dig(0, 'mailbox')
    end
  end
  # Truc de ruby pour capturer les erreurs
rescue => e
  # Si y'a une erreur, on se déconnecte
  imap.logout
end
# Une fois fini, on se déconnecte
imap.logout
